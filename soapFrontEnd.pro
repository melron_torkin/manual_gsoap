#-------------------------------------------------
#
# Project created by QtCreator 2015-05-24T17:46:22
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = soapFrontEnd
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    soapClientLib.cpp \
    envC.cpp

HEADERS  += mainwindow.h \
    AddServiceSoapBinding.nsmap \
    soapH.h \
    soapStub.h

FORMS    += mainwindow.ui

LIBS +=-lgsoap++ -lgsoapssl++ -lgsoapck++
