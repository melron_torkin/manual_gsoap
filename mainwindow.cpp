#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "soapStub.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect( ui->add_button, SIGNAL(clicked()), this, SLOT(OnAddButtonClicked()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::OnAddButtonClicked()
{
    double y=0.0;
    double x=0.0;
    double res;

    soap soap_str;
    char* chr = new char(15);

    bool ok=true;
    x = ui->x_value->text().toDouble(&ok);
    if( !ok )
    {
        ui->result->setText("Error");
        return;
    }
    y = ui->y_value->text().toDouble(&ok);
    if( !ok )
    {
        ui->result->setText("Error");
        return;
    }
    if( soap_call_ns1__func(&soap_str, "http://localhost:8080/AddService.jws",chr, x, y, res ) == SOAP_OK )
    {
        ui->result->setText( QString::number(res) );
    }
    else
        ui->result->setText("Error");
}
